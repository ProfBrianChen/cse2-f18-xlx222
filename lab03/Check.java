///////////////
/// Xin Ling Xu CSE002
/// lab03

// uses Scanner class to import information from an existing class and use that to make calculations
// calculates percentage tip, number of ways the check will be split and each person's share of the check

import java.util.Scanner; 
  //import statement
 public class Check{
   //main method required for every java program
   public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); 
    System.out.print("Enter the original cost od the check (in the form xx.xx);");
      double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole nuber (in the form xx):");
      double tipPercent = myScanner.nextDouble();
      tipPercent/=100; //conversion of the percentage to a decimal value
    System.out.print("Enter the numvber of people wh went out to dinner;");
     int numPeople = myScanner.nextInt();
     double totalCost; 
     double costPerPerson;
     int dollars, dimes, pennies; 
     totalCost = checkCost*(1+tipPercent);
     costPerPerson = totalCost/numPeople;
     //drops decimal fraction
     dollars=(int)costPerPerson; 
     //get dimes amount,
     //(int)(6.73*10)%10-> 67% 10-> 73
     // where the % (mod) operator returnes the remainder
     // after the division: 583%100-> 83, 27%5-> 27
    dimes=(int)(costPerPerson*10)%10;
    pennies=(int)(costPerPerson*100)%10;
   System.out.println("Each person in the group owes $"+ dollars + '.' + dimes + pennies);
     
   } //end of main method 
 } //end of class