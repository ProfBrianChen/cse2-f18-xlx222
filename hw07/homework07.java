////////////////
//Xin Ling Xu 
//CSE002-HW07

//import statement for the scanner class
import java.util.Scanner;

public class homework07 {
	// main method
	public static void main(String[] args) {
		System.out.print("Enter the input text:");
		String text = sampleText(); // Get user input (text)
		boolean truth = true;
		
		while (truth) {
			
			printMenu(); // called the menu method and prints the options from the menu
			
			Scanner scan = new Scanner(System.in);
			String input = scan.next(); // gets the option chosen by the user from the menu
			
			if (input.equals("q")) { // option q is to quit therefore break out of loop
				truth = false;
			}
			
			else if (input.equals("c")) { 
				int count = getNumOfNonWSCharacters(text); // option c calls on the method
				System.out.println("Number of characters (non-white spaces): " + count);
				truth = false;
			}
			
			else if (input.equals("w")) {
				int count = getNumOfWords(text);
				System.out.println("Number of words: " + count);
				truth = false;
			}
			
			else if (input.equals("f")) {
				System.out.println("Enter the word you want to find: ");
				String toFind = scan.next();
				int count = findText(toFind, text);
				System.out.println("Number of occurences: " + count);
				truth = false;
			} 
			
			else if (input.equals("r")) {
				String fix = replaceExclamation(text);
				System.out.println(fix);
				truth = false;
			} 
			
			else if (input.equals("s")) {
				String fix = shortenSpace(text);
				System.out.println(fix);
				truth = false;
			} 
			
			else {
				System.out.println("The option you've chosen is not valid");
			}
			
		}
	}
	
	public static String sampleText() {
		Scanner scan = new Scanner(System.in);
    
		String input = scan.nextLine(); // Store what the user input
		System.out.println("You entered: " + input);
		return input;
	}
	
	public static void printMenu() {
		System.out.println("MENU");
		
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
	}
	
	// Return the number of non-whitespace characters
	public static int getNumOfNonWSCharacters(String s) {
		
		int count = 0; // number of characters 
	
		for (int i = 0 ; i < s.length(); i++) {
			char letter = s.charAt(i);
			
			// Is the letter a whitespace?
			if (letter != ' ') {
				count++;
			}
		}
		
		return count;
	}
	
	public static int getNumOfWords(String s) {
		int count = 0;
		
		for (int i = 0; i < s.length(); i++) {
			char letter = s.charAt(i);
			if (letter == ' ') {
				count++;
			}
				
		}
		
		return count + 1;
	}
	
	public static int findText(String toFind, String text) {
		
		int count = 0; // number of words found
		int index = 0; // letters of the word found so far?
		
		for (int i = 0; i < text.length(); i++) {
			char currentLetter = text.charAt(i);
			char weNeed = toFind.charAt(index);
			
			// Doesn't match!
			if (weNeed != currentLetter) {
				index = 0;
			}
			
			// Matches!
			else {
				index++;
			}
			
			// We found one instance of the word
			if (index == toFind.length()) {
				count++;
				index = 0;
			}	
		}
		
		return count;
	}
	
	public static String replaceExclamation(String s) {
		return s.replace('!', '.'); // replaces any exclamations with periods
	}
	
	
	public static String shortenSpace(String s) {
		String retVal = "";
		
		for (int i = 0; i < s.length(); i++) {
			char letter = s.charAt(i);
			
			//If you see a whitespace, keep on going until you don't see it again
			if (letter == ' ') {
				retVal += letter;
				
				while (letter == ' ') { // loop inside the if statement to keep looping untill the next non-space is found
					i++;
					letter = s.charAt(i);
				}
			}
			retVal += letter; 
		}
		return retVal;
  }
}