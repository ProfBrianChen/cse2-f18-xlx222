////////////
////// CSE 002 Xin Xu 
/// hw02 Arithmetic
//  computing the cost of items with the state tax of 6%
public class Arithmetic { 
  //mainmethod required for every java program 
  public static void main(String[]args) {
   //// Assumptions (input variable)
   //// (the input variables)  
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt 
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //Cost per belt
    double beltCost = 33.99; 
    //the tax rate
    double paSalesTax =0.06;
    
    //calcultions are below
    double totalCostOfPants; //total cost of pants without tax
     totalCostOfPants=(numPants*pantsPrice);
     double salesTaxOfPants; //tax of pants
     salesTaxOfPants=(int)((totalCostOfPants*paSalesTax)*100.0)/100.0;
     double pantsPriceTaxed; //total cost of pants with tax
     pantsPriceTaxed=(int)((totalCostOfPants+salesTaxOfPants)*100.0)/100.0;
   System.out.println("total cost of pants without tax is "+ totalCostOfPants +" dollars");
   System.out.println("total cost of pants with tax is "+ pantsPriceTaxed +" dollars");
   System.out.println("sales tax on pants is "+ salesTaxOfPants +" dollars");
    
    
    double totalCostOfShirts; //total cost of shirts without tax
     totalCostOfShirts=(numShirts*shirtPrice);
     double salesTaxOfShirts; //tax of shirts
     salesTaxOfShirts=(int)((totalCostOfShirts*paSalesTax)*100.0)/100.0;
     double shirtsPriceTaxed; //total cost of shirts with tax
     shirtsPriceTaxed=(int)((totalCostOfShirts+salesTaxOfShirts)*100.0)/100.0;
   System.out.println("total cost of shirts without tax is "+ totalCostOfShirts +" dollars");
   System.out.println("total cost of shirts with tax is "+ shirtsPriceTaxed +" dollars");
   System.out.println("sales tax on shirts is "+ salesTaxOfShirts +" dollars");
    
    double totalCostOfBelts; //total cost of belts without tax
     totalCostOfBelts=(numBelts*beltCost);
     double salesTaxOfBelts; //tax of belts
     salesTaxOfBelts=(int)((totalCostOfBelts*paSalesTax)*100.0)/100.0;
     double beltsPriceTaxed; //total cost of belts with tax
     beltsPriceTaxed=(int)((totalCostOfBelts+salesTaxOfBelts)*100.0)/100.0;
   System.out.println("total cost of belts without tax is "+ totalCostOfBelts +" dollars");
   System.out.println("total cost of belts with tax is "+ beltsPriceTaxed +" dollars");
   System.out.println("sales tax on belts is "+ salesTaxOfBelts +" dollars");
    
    double totalCostOfPurchase; //total cost of purchase without tax
    totalCostOfPurchase=(totalCostOfPants+totalCostOfShirts+totalCostOfBelts);
   System.out.println("total cost of purchase without tax is "+ totalCostOfPurchase +" dollars");
    
    double totalSalesTax; //total sales tax
    totalSalesTax=(salesTaxOfPants+salesTaxOfShirts+salesTaxOfBelts);
   System.out.println("total sales tax is "+ totalSalesTax +" dollars");
    
    double totalPriceOfTransaction; //total paid for the transaction (cost of purchase including tax) 
    totalPriceOfTransaction=(totalCostOfPurchase+totalSalesTax); //total cost of purchase with tax
   System.out.println("total cost of purchase with tax is "+ totalPriceOfTransaction +" dollars");
    
  } //end of main method
}     
