/////////////
// Xin ling Xu 
// CSE002 - HW06

import java.util.Scanner;

public class EncryptedX{
public static void main(String[]args){
Scanner myScanner = new Scanner (System.in); // declares and initializes the scanner
  // askes for user input 
  System.out.println("Enter an integer between 0 and 100 to indicate the size of the square");
  int input = myScanner.nextInt(); // takes user input
  
  // checks if the user input is an integer between 0 and 100
  boolean correct = true; // allows the while loop to always run
    while (correct){
      if (input >= 0 && input <= 100){ // checks if input is in range 
        correct= false; // if valid, this breaks out of the while loop
      }
      else { // generates a error message is the integer is not btween 0 and 100 inclusively (not valid)
        System.out.println("The number you've entered is not within range!");
        System.out.println("Please enter another integer that is valid: ");
        input = myScanner.nextInt();
      } 
      // no incrementations is added, therefore the loop becomes an infinite loop
    } // end of while loop
  
  
  // generated the output using the input data
   for (int a = 0; a < input; a++ ){ // generates the number of rows
     for ( int b = 0; b < input; b++ ){ // genertes the numbers per row
       if ( a == b ){
         System.out.print(" "); // prints one horizontal of the secret message 
       }
       if ( b == (input-1)-a ){
         System.out.print(" "); // print the other horizontal of the secret message 
       }
       else {
         System.out.print("*"); // print the area in which the secret message will be carved out from
       }
     }
     System.out.println(""); // prints nothing and allows the for loop to go to the next line 
   } // end of the for loop 
   


} // end of main method
} // end of class