///////////
// Xinling Xu
// CSE002-HW05

import java.util.Scanner;// imports scanner

public class Hw05{ 
  public static void main (String[]args){
   Scanner myScanner = new Scanner(System.in);
   System.out.println("How many times should I generate hands?");
   int times = myScanner.nextInt();
   int copy = times;
   int fourKind = 0;
   int threeKind = 0;
   int twoPair = 0;
   int onePair =0;
    
       
    while (times != 0){
      
      int number1 = (int)(Math.random()*52+1);
      int number2 = (int)(Math.random()*52+1);
      while (number1==number2){
        number2 = (int)(Math.random()*52+1);
      }
      int number3 = (int)(Math.random()*52+1);
      while (number1 == number3 || number2 == number3){
        number3 = (int)(Math.random()*52+1);
      }
      int number4 = (int)(Math.random()*52+1);
      while (number1 == number4 || number2 == number4 || number3 == number4){
        number4 = (int)(Math.random()*52+1);
      }
      int number5 = (int)(Math.random()*52+1);
       while (number1 == number5 || number2 == number5 || number3 == number5 || number4 == number5){
        number5 = (int)(Math.random()*52+1);
      }
       
  
    	
       System.out.println(number1 + " " + number2 + " " + number3 + " " + number4 + " " + number5);
    
      // four of a kind
     if ((number1 % 13 == number2 % 13 && number2 % 13 == number3 % 13 && number3 % 13 == number4 % 13) || 
         (number2 % 13 == number3 % 13 && number3 % 13 == number4 % 13 && number4 % 13 == number5 % 13) ||
         (number3 % 13 == number4 % 13 && number4 % 13 == number5 % 13 && number5 % 13 == number1 % 13) ||
         (number4 % 13 == number5 % 13 && number5 % 13 == number1 % 13 && number1 % 13 == number2 % 13) ||
         (number5 % 13 == number1 % 13 && number1 % 13 == number2 % 13 && number2 % 13 == number3 % 13)){
       fourKind++;
     }
      
      //  three of a kind
      if ((number1 % 13 == number2 % 13 && number2 % 13 == number3 % 13) ||
          (number2 % 13 == number3 % 13 && number3 % 13 == number4 % 13) ||
          (number3 % 13 == number4 % 13 && number4 % 13 == number5 % 13) ||
          (number4 % 13 == number5 % 13 && number5 % 13 == number1 % 13) ||
          (number5 % 13 == number1 % 13 && number1 % 13 == number2 % 13) ||
          (number1 % 13 == number3 % 13 && number3 % 13 == number3 % 13) ||
          (number1 % 13 == number2 % 13 && number2 % 13 == number4 % 13) ||
          (number2 % 13 == number3 % 13 && number3 % 13 == number5 % 13) ||
          (number2 % 13 == number4 % 13 && number4 % 13 == number5 % 13) ||
          (number1 % 13 == number3 % 13 && number3 % 13 == number5 % 13)){

        threeKind++;
      }
          
     // two pair
      if ((number1 % 13 == number2 % 13 && number3 % 13 == number4 % 13)||
          (number1 % 13 == number2 % 13 && number3 % 13 == number5 % 13)||
          (number1 % 13 == number2 % 13 && number4 % 13 == number5 % 13)||
          (number1 % 13 == number3 % 13 && number2 % 13 == number4 % 13)||
          (number1 % 13 == number3 % 13 && number2 % 13 == number5 % 13)||
          (number1 % 13 == number3 % 13 && number4 % 13 == number5 % 13)||
          (number1 % 13 == number4 % 13 && number2 % 13 == number3 % 13)||
          (number1 % 13 == number4 % 13 && number2 % 13 == number5 % 13)||
          (number1 % 13 == number4 % 13 && number3 % 13 == number5 % 13)||
          (number1 % 13 == number5 % 13 && number2 % 13 == number3 % 13)||
          (number1 % 13 == number5 % 13 && number2 % 13 == number4 % 13)||
          (number1 % 13 == number5 % 13 && number3 % 13 == number4 % 13)||
          (number2 % 13 == number3 % 13 && number4 % 13 == number5 % 13)||
          (number3 % 13 == number4 % 13 && number2 % 13 == number5 % 13)) {

         twoPair++;
      }
    	  
      // one pair
      if ((number1 % 13 == number2 % 13 && number3 % 13 != number4 % 13 && number4 % 13 != number5 % 13 && number3 % 13 != number5 % 13)||
          (number1 % 13 == number3 % 13 && number2 % 13 != number4 % 13 && number4 % 13 != number5 % 13 && number2 % 13 != number5 % 13)||
          (number1 % 13 == number4 % 13 && number2 % 13 != number3 % 13 && number3 % 13 != number5 % 13 && number2 % 13 != number5 % 13)||
          (number1 % 13 == number5 % 13 && number2 % 13 != number3 % 13 && number3 % 13 != number4 % 13 && number2 % 13 != number4 % 13)||
          (number2 % 13 == number3 % 13 && number1 % 13 != number4 % 13 && number4 % 13 != number5 % 13 && number1 % 13 != number5 % 13)||
          (number2 % 13 == number4 % 13 && number1 % 13 != number3 % 13 && number3 % 13 != number5 % 13 && number1 % 13 != number5 % 13)||
          (number2 % 13 == number5 % 13 && number1 % 13 != number3 % 13 && number3 % 13 != number4 % 13 && number1 % 13 != number4 % 13)||
          (number3 % 13 == number4 % 13 && number1 % 13 != number2 % 13 && number2 % 13 != number5 % 13 && number1 % 13 != number5 % 13)||
          (number3 % 13 == number5 % 13 && number1 % 13 != number2 % 13 && number2 % 13 != number4 % 13 && number1 % 13 != number4 % 13)||
          (number4 % 13 == number5 % 13 && number1 % 13 != number2 % 13 && number2 % 13 != number3 % 13 && number1 % 13 != number3 % 13)){

    	  onePair++;
      }
          

      times--;
    }
    
    //calculating probability
    double fourKindProb = (double)fourKind/copy;
    double threeKindProb = (double)threeKind/copy;
    double twoPairProb = (double)twoPair/copy;
    double onePairProb = (double)onePair/copy;
    

     System.out.println("The number of hands drawn is " + copy);
     System.out.printf("The probability of four of a kind is %.3f\n", fourKindProb);
     System.out.printf("The probability of three of a kind is %.3f\n", threeKindProb);
     System.out.printf("The probability of two pairs is %.3f\n", twoPairProb);
     System.out.printf("The probability of one pair is %.3f\n", onePairProb);
     
   
}  // end of main method

}// end of class