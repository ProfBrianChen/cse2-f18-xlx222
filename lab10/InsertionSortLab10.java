/////////////
// XIN LING XU
// CSE002-LAb10
// DEC 06

import java.util.Arrays; // import statement for arrays

public class InsertionSortLab10{
  public static void main (String[] args){
    
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    int iterBest = insertionSort(myArrayBest);
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
     
  }// end of main  method
  
  
  // the method for sorting the numbers
  public static int insertionSort(int[] list){
    System.out.println(Arrays.toString(list));
    int iterations = 0; // initialized the variable iterations
    
    for(int i = 1; i < list.length; i++){
       iterations++;
     for(int j = i; j > 0; j--){
       if(list[j] < list[j-1]){
       int storage = list[j];
       list[j] = list[j-1];
       list[j-1] = storage;
       iterations++;
     }// end of if statement
       else{
         break;
       }
     }// end of inner for loop
      System.out.println(Arrays.toString(list));
    }// end of outer for loop
    return iterations;
    
  }// end of insertionsort method 
  
  
}// end of class
      