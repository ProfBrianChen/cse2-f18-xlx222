/////////////
// XIN LING XU
// CSE002-LAb10
// DEC 06

import java.util.Arrays; // import statement for arrays

public class SelectionSortLab10{
  public static void main (String[] args){
    
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    int iterBest = selectionSort(myArrayBest);
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
    
  }// end of main  method
  
  
  // the method for sorting the numbers
  public static int selectionSort(int[] list){
    System.out.println(Arrays.toString(list));
    int iterations = 0; // initialized the variable iterations
    
    for(int i = 0; i < list.length-1; i++){
      iterations++;
      int currentMin = list[i];
      int currentMinIndex = 1;
      for(int j = i+1; j < list.length; j++){
        if (currentMin > list[j]){
          currentMin = list[j];
          currentMinIndex = j;
         } 
        iterations++;
      }// end of inner for loop that compares j
      if (currentMinIndex != i){
        int contain = list[i];
        list[i] = list[i+1];
        list[i+1] = contain; 
      }
    } // end of outer loop that counts iterations and i
    return iterations;
  } //end of selectionsort method    
    
    
  
} // end of class