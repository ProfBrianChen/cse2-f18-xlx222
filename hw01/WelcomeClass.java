//////////////
////// CSE 02 hw01 WelcomeClass
//// Xin Ling Xu 
///
public class WelcomeClass{
  
   public static void main(String args[]){
     ///prints messages to terminal window 
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-X--L--X--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
     
   }
  
 }
