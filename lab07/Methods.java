/////////////
// Xin Ling Xu 
// CSE002-LAB07

import java.util.Random; // import statement of the random class
import java. util.Scanner; //import scanner

public class Methods{
  public static void main {Strings[]args}{
  Scanner myScanner = new Scanner (System.in);
    System.out.println("Please enter '1' to start the sentence generator");
      int a = myScanner.nextInt(); 
    boolean correct = true
     while(correct){
     
    // prints adjectives
       public static String printAdjective(int a, int b){
         Random randomGenerator = new Random();
         int randomInt = randomGenerator.nextInt(10);
          switch (randomInt){
           case 0:
             System.out.println("quick");
             break;
           case 1:
             System.out.println("brown");
             break;
           case 2:
             System.out.println("lazy");
             break; 
           case 3:
             System.out.println("small");
             break; 
           case 4:
             System.out.println("boring");
             break;
           case 5:
             System.out.println("public");
             break;
           case 6:
             System.out.println("superifcial");
             break; 
           case 7:
             System.out.println("unique");
             break; 
           case 8:
             System.out.println("cute");
             break; 
           case 9:
             System.out.println("signficant");
             break;
          } // end of switch 
         } // end of method
    
   // prints subject nouns    
       public static String printSubjectNoun(int a. int b){
         Random randomGenerator = new Random();
         int randomInt = randomGenerator.nextInt(10);
          switch (randomInt){
           case 0:
             System.out.println("panda");
             break;
           case 1:
             System.out.println("baby");
             break;
           case 2:
             System.out.println("doggy");
             break; 
           case 3:
             System.out.println("shark");
             break; 
           case 4:
             System.out.println("monkey");
             break;
           case 5:
             System.out.println("dragon");
             break;
           case 6:
             System.out.println("tiger");
             break; 
           case 7:
             System.out.println("ant");
             break; 
           case 8:
             System.out.println("kitty");
             break; 
           case 9:
             System.out.println("sloth");
             break;
         } // end of switch 
       } // end of method
      
    // prints past tense verbs 
       public static String printPastVerb(int a, int b){
         Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
          switch (randomInt){
           case 0:
             System.out.println("bit");
             break;
           case 1:
             System.out.println("smacked");
             break;
           case 2:
             System.out.println("ran");
             break; 
           case 3:
             System.out.println("pushed");
             break; 
           case 4:
             System.out.println("brought");
             break;
           case 5:
             System.out.println("caressed");
             break;
           case 6:
             System.out.println("made");
             break; 
           case 7:
             System.out.println("picked");
             break; 
           case 8:
             System.out.println("walked");
             break; 
           case 9:
             System.out.println("snuck");
             break;
         } // end of switch
        } // end of method
    
    // prints object nouns  
       public static String printObjectNoun(int a, int b){
         Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
          switch (randomInt){
           case 0:
             System.out.println("lamp");
             break;
           case 1:
             System.out.println("cup");
             break;
           case 2:
             System.out.println("table");
             break; 
           case 3:
             System.out.println("building");
             break; 
           case 4:
             System.out.println("bridge");
             break;
           case 5:
             System.out.println("stove");
             break;
           case 6:
             System.out.println("tree");
             break; 
           case 7:
             System.out.println("bush");
             break; 
           case 8:
             System.out.println("fortress");
             break; 
           case 9:
             System.out.println("rock");
             break;
         } // end of switch 
        } // end of method
       
       
       break; // breaking out of the while loop
     } // end of while loop
    
   
    
  } // end of main method
} // end of class

  
                    
