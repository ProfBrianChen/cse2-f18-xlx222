/////////////
// Xin Ling Xu 
// CSE002-LAB07

import java.util.Random; // import statement of the random class
import java. util.Scanner; //import scanner

public class lab07{
  public static void main (String [] args) {
  Scanner myScanner = new Scanner(System.in);
    boolean correct = true;
    
      
     while (correct){
     
       finalSentence();
       
       System.out.println("Enter YES if you want to quit? Otherwise another sentence will be formed");
       
       String input = myScanner.next();
       
       if (input.equals("YES")) {
         correct = false;
       }   
       
     } // end of while loop
    
   
    
  } // end of main method
  
       public static String printAdjective(){
         Random randomGenerator = new Random();
         int randomInt = randomGenerator.nextInt(10);
                  
          switch (randomInt){
           case 0:
             return "quick";
              
           case 1:
             return "brown";
               
           case 2:
             return "lazy";
               
           case 3:
             return "small";
               
           case 4:
             return "boring";
              
           case 5:
             return "public";
             
           case 6:
             return "big";
         
           case 7:
             return "kinky";
             
           case 8:
             return "cute";
              
           case 9:
             return "ugly";
              
            default:
              return "smack";
          } // end of switch 
         
         
         
         } // end of method
  
  
  // prints subject nouns    
       public static String printSubjectNoun(){
         Random randomGenerator = new Random();
         int randomInt = randomGenerator.nextInt(10);
          switch (randomInt){
           case 0:
             return "panda";
              
           case 1:
             return "baby";
              
           case 2:
              return "unicorn";
              
           case 3:
              return "shark";
              
           case 4:
              return "monkey";
              
           case 5:
              return "dragon";
              
            case 6:
             return "tiger"; 
              
            case 7:
              return "ant";
              
           case 8:
             return "kitty";
              
           case 9:
             return "sloth";
              
            default:
              return "racoon";
         } // end of switch 
       } // end of method
  
  
  
  // prints past tense verbs 
       public static String printPastVerb(){
         Random randomGenerator = new Random();
         int randomInt = randomGenerator.nextInt(10);
         
          switch (randomInt){
           case 0:
             return "bit";
           case 1:
             return "smacked";
           case 2:
             return "ran";
           case 3:
             return "pushed";
           case 4:
             return "brought";
           case 5:
             return "caressed";
           case 6:
             return "made";
           case 7:
             return "picked";
           case 8:
             return "walked";
           case 9:
             return "snuck";
              
            default:
              return "farted";
         } // end of switch
        } // end of method
  
  
  
  // prints object nouns  
       public static String printObjectNoun() {
         Random randomGenerator = new Random();
         int randomInt = randomGenerator.nextInt(10);
         
          switch (randomInt){
           case 0:
             return "lamp";
           case 1:
              return "sofa";
           case 2:
             return "table";
           case 3:
             return "building";
           case 4:
             return "bridge";
           case 5:
             return "stove";
           case 6:
              return "tree";
           case 7:
             return "bush";
           case 8:
              return "fortress";
           case 9:
              return "rock";
              
            default:
              return "stone";
         } // end of switch 
        } // end of method
  
  
  public static String formSentence() {
    String subject = printSubjectNoun();
       String subjectAdjective = printAdjective();
       
       String verb = printPastVerb();
       
       String secondSubject = printObjectNoun();
       String secondsubjectAdjective = printAdjective();
       
       System.out.println("The " + subjectAdjective + " " + subject + " " + verb + " the " + secondsubjectAdjective + " " + secondSubject);
    
    return "The " + subject;
  }
  
  public static void formSentenceTwo(String subject) {
    String firstSubject = printSubjectNoun();
    String verb = printPastVerb();
    
    String secondSubject = printSubjectNoun();
    String adjective = printAdjective();
    String thirdSubject = printObjectNoun();
    
    System.out.println(subject + " " + "shoved a " + firstSubject + " to " + verb + " " + secondSubject + " at the " + adjective + " " + thirdSubject);
    
    
    
    
  }
  
  public static void formSentenceThree(String subject) {
    String verb = printPastVerb();
    String objectSubject = printObjectNoun();
    
    System.out.println(subject + " " + verb + " that " + objectSubject);
  }
  
  public static void finalSentence() {
    String subject = formSentence();
    formSentenceTwo(subject);
    formSentenceThree(subject);
  }
    
} // end of class

  
                    
