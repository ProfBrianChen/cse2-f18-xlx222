///////////////
//Xin Ling Xu
// CSE002-LAB08
// November 8th, 2018

import java.lang.Math; //imports random number generator

public class Arrays{
public static void main (String []args){
        // creates two arrays each having a size of 100
        int[] array1 = new int[100];
        int[] array2 = new int[100];
       
        for (int i = 0; i < array1.length; i++){ // loop one with the length of array1
            array1[i] = (int)(Math.random()*100+1); // assigns a random number to the member of the array
            System.out.print(array1[i] + " "); //prints the random number at the member with a space 
        } // end of loop 1
        
        for (int i = 0; i < array2.length; i++){ // loop two that "counts" the occurance 
            int k = array1[i]; // save the array1 outcome to an integer
            array2[k]++; // passes the outcome of array one into two and increments it if array1=array2
        } // end of loop 2
        
        for (int i = 0; i < array2.length; i++){
            if (i == 1){
               System.out.print(i + " occurs " + array2[i] + " time.");
               System.out.println("");
             }
            else {
               System.out.print(i + " occurs " + array2[i] + " times.");
               System.out.println("");
             }
            } // the end of the printing loop to show occurance
       
    } // end of main method
} // end of class