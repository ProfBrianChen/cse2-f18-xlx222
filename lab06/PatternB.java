////////////////
// Xin Ling Xu 
//CSE002-LAB06
// OCTOBER 11TH, 2018

import java.util.Scanner; //imports scanner

public class PatternB{
 public static void main (String[]args){
  Scanner myScanner = new Scanner (System.in); //initialize scanner
  System.out.println("Enter an interger between 1-10: ");
  int input= myScanner.nextInt(); // takes the next input as a integer
   boolean correct= true;
   while(correct){
     if(input>=1 && input<=10){ //checks if the input is within the range
       correct=false;
     }
       else{ //yields an error message if input is not in range
          System.out.println("The number you've enter is not valid!");
          System.out.println("please enter another number");
            input=myScanner.nextInt();
       }
     }
   
  for(int numRows = input; numRows >= 1; numRows--){ //compares input with numRows
     for (int numbers = 1; numbers <= numRows; numbers++){ //compares numRows with numbers
       System.out.print(numbers);
     }
   System.out.println(" ");
  }
 
    
    
 }// end of main method 
} // end of class