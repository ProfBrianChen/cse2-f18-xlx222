////////////
// Xin Ling Xu 
// CSE002-HW04
// This is a different approach to hw04 using switch statements
// This is the beginning of a terribly long period of time spent on hw04

import java.util.Scanner; //imports scanner
import java.lang.Math; //allows for the usage of math.random

public class CrapsSwitch{
  public static void main(String[]args){
    Scanner myScanner= new Scanner (System.in);
    
  System.out.println("Enter 0 if you'd like to enter numbers to be evaluated and 1 for random dice outcomes");
    int choice= myScanner.nextInt(); 
    int number1= (int)(Math.random()*6+1);
    int number2= (int)(Math.random()*6+1);
    
String identity="";
    
   switch(choice){
     case 0:
       System.out.print("Enter the first number you would like to evaluate: ");
         number1=myScanner.nextInt();
       
       System.out.print("Enter the second number you would like to evaluate: ");
        number2=myScanner.nextInt();
       
       switch(number1){
         case 1:
           switch (number2){
            case 1:
              identity="Snake Eyes";
              break;
            case 2:
              identity="Ace Deuce";
              break;
            case 3:
              identity="Easy Four";
              break;
            case 4:
              identity="Fever Five";
              break;
            case 5:
              identity="Easy Six";
              break;
            case 6:
              identity="Seven out";
              break;
            default:
              System.out.println("There seems to be an error");
               break;
            } break;
           // end of choice 0 case 1
         case 2:
          switch (number2){
            case 1:
              identity="Ace Deuce";
              break;
            case 2:
              identity="Hard four";
              break;
            case 3:
              identity="Fever";
              break;
            case 4:
              identity="Easy six";
              break;
            case 5:
              identity="Seven out";
              break;
            case 6:
              identity="Easy eight";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } break;
           // end of choice 0 case 2
       case 3:
          switch (number2){
            case 1:
              identity="Easy four";
              break;
            case 2:
              identity="Fever five";
              break;
            case 3:
              identity="Hard six";
              break;
            case 4:
              identity="Seven out";
              break;
            case 5:
              identity="Easy eight";
              break;
            case 6:
              identity="Nine";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } break; 
           // end of choice 0 case 3
       case 4:
          switch (number2){
            case 1:
              identity="Fever five";
              break;
            case 2:
              identity="Easy six";
              break;
            case 3:
              identity="Seven out";
              break;
            case 4:
              identity="Hard eight";
              break;
            case 5:
              identity="Nine";
              break;
            case 6:
              identity="Easy ten";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } break; 
           //end of choice 0 case 4
      case 5:
          switch (number2){
            case 1:
              identity="Easy six";
              break;
            case 2:
              identity="Seven out";
              break;
            case 3:
              identity="Easy eight";
              break;
            case 4:
              identity="Nine";
              break;
            case 5:
              identity="Hard ten";
              break;
            case 6:
              identity="Yo-leven";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } break;
           // end of choice 0 case 5
      case 6:
          switch (number2){
            case 1:
              identity="Seven out";
              break;
            case 2:
              identity="Easy eight";
              break;
            case 3:
              identity="Nine";
              break;
            case 4:
              identity="FEasy ten";
              break;
            case 5:
              identity="Yo-leven";
              break;
            case 6:
              identity="Boxcars";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } //end of choice 0 case 6
          break;
           
     default:
       System.out.println("There seems to be an error");
           break;
       } break; // end of choice 0
       
      
      case 1:
         switch(number1){
         case 1:
          switch (number2){
            case 1:
              identity="Snake Eyes";
              break;
            case 2:
              identity="Ace Deuce";
              break;
            case 3:
              identity="Easy Four";
              break;
            case 4:
              identity="Fever Five";
              break;
            case 5:
              identity="Easy Six";
              break;
            case 6:
              identity="Seven out";
            default:
              System.out.println("There seems to be an error");
              break;
          } break;
             // end of choice 1 case 1
        case 2:
          switch (number2){
            case 1:
              identity="Ace Deuce";
              break;
            case 2:
              identity="Hard four";
              break;
            case 3:
              identity="Fever";
              break;
            case 4:
              identity="Easy six";
              break;
            case 5:
              identity="Seven out";
              break;
            case 6:
              identity="Easy eight";
            default:
              System.out.println("There seems to be an error");
              break;
          } break;
             // end of choice 1 case 2
       case 3:
          switch (number2){
            case 1:
              identity="Easy four";
              break;
            case 2:
              identity="Fever five";
              break;
            case 3:
              identity="Hard six";
              break;
            case 4:
              identity="Seven out";
              break;
            case 5:
              identity="Easy eight";
              break;
            case 6:
              identity="Nine";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } break;
             // end of choice 1 case 3
       case 4:
          switch (number2){
            case 1:
              identity="Fever five";
              break;
            case 2:
              identity="Easy six";
              break;
            case 3:
              identity="Seven out";
              break;
            case 4:
              identity="Hard eight";
              break;
            case 5:
              identity="Nine";
              break;
            case 6:
              identity="Easy ten";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } break;
             // end of choice 1 case 4
      case 5:
          switch (number2){
            case 1:
              identity="Easy six";
              break;
            case 2:
              identity="Seven out";
              break;
            case 3:
              identity="Easy eight";
              break;
            case 4:
              identity="Nine";
              break;
            case 5:
              identity="Hard ten";
              break;
            case 6:
              identity="Yo-leven";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } break;
             // end of choice 1 case 5
      case 6:
          switch (number2){
            case 1:
              identity="Seven out";
              break;
            case 2:
              identity="Easy eight";
              break;
            case 3:
              identity="Nine";
              break;
            case 4:
              identity="Easy ten";
              break;
            case 5:
              identity="Yo-leven";
              break;
            case 6:
              identity="Boxcars";
              break;
            default:
              System.out.println("There seems to be an error");
              break;
          } break;
             // end of choice 1 case 6
       } // end of case 1
       break;
   } // end of choice 
  
    
    System.out.println("The first roll is "+ number1);
    System.out.println("The second roll is "+ number2);
    
    System.out.println("The outcome of your roll is "+ identity );
    
        
        
  } //end of main method
}//end of main class
