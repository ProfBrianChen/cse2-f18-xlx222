///////////////
/// Xin Ling Xu CSE002
/// hw03

// uses Scanner class to import information from an existing class and use that to make calculations

import java.util.Scanner; 
  //import statement
 public class Convert {
   //main method required for every java program
   public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); 
    System.out.print("Enter the affected area in acres ");
      double affectedArea = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected area ");
      double affectedRainfall = myScanner.nextDouble();
     
   double rainInAcres =affectedArea*affectedRainfall;
   double rainInGallons =rainInAcres*27154.2857;
   double rainInMiles =rainInGallons*9.08169e-13;
   System.out.print("The quantity of rain is "+rainInMiles+" cubic miles"); 
    
   } //end of main method
 } //end of class
