///////////////
/// Xin Ling Xu CSE002
/// hw03

// uses Scanner class to import information from an existing class and use that to make calculations

import java.util.Scanner; 
  //import statement
 public class Pyramid {
   //main method required for every java program
   public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); 
    System.out.print("The square side of the pyramid is (input lenth): ");
      double squareSide = myScanner.nextDouble();
    System.out.print("The height of the pyramid is (input height): ");
      double pyramidHeight = myScanner.nextDouble();
   
   double pyramidArea=squareSide*squareSide;  
   double pyramidVolume=(pyramidHeight/3)*pyramidArea;
   System.out.print("The volume inside the pyramid is: "+ pyramidVolume +""); 
    
   } //end of main method
 } //end of class

