//////////////////
// CSE002 -HW08
// XIN LING XU 
// NOV.8.18

import java.util.Scanner;
import java.lang.Math;

public class Homework08{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  // System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
   if (index<5){
       index = 51;
       shuffle(cards);
   }
}  
  }
  
  public static void printArray( String[] cards){
    for(int i = 0; i < cards.length; i++){
        System.out.print(cards[i]+" ");
    }
    System.out.println("");
  }
  
  public static void shuffle(String[] cards){
    for(int j = 0; j < 100; j++){
        int index = (int) (Math.random()*51+1);
        String k = cards[0];
        cards[0] = cards[index];
        cards[index] = k;
        
    }  
  }
  
  public static String[] getHand(String[] cards, int index, int numCards){
     String[] hand = new String[numCards];
     if (numCards > index+1){
       String[] suitNames={"C","H","S","D"};    
       String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
      for (int i=0; i<52; i++){ 
         cards[i]=rankNames[i%13]+suitNames[i/13]; 
          } 
     }
     for (int i = 0; i < numCards; i++){
         hand[i]= cards[index-i];
     }
    return hand;
  }
}
