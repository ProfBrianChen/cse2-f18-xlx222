//////////////////
// CSE002 -HW09
// XIN LING XU 
// NOV.16.18

import java.util.Scanner; // import statement
import java.lang.Math; // import statement 

public class CSE2Linear{ 
public static void main(String[] args) { 
  System.out.println("Enter 15 ascending integers for final grades in CSE2");
   int[] grades = new int[15];
   int i = 0;
   int grade = 0;
   Scanner myScanner = new Scanner(System.in);
   while (i < 15){ 
    if (myScanner.hasNextInt()){
       grade = myScanner.nextInt();
      if ( grade > 100 || 0 > grade){
       System.out.println("The number you've entered is not between 0 and 100");
      continue;
        }
      if (i !=0){  
         if(grade < grades[i-1]){
          System.out.println("the number you've entered is not greater than the prior number please try again");
      continue;
        }
         continue;
      }
   grades[i] = grade;
    }
    else{
      System.out.println("The number you've entered is not an integer, please try again");
      continue;
    }
    
   //grades[i] = grade;
    i++;
  } // end of while loop
  
for (int x = 0; x < 15; x++){
  System.out.print(grades[x] +" ");
}
 
 System.out.println("Enter a grade to search for");
   int find = myScanner.nextInt();
     binarySearch(grades, find);
     randomScrabbling(grades);
  
 for (int x = 0; x < 15; x++){
  System.out.print(grades[x] +" ");
}
  System.out.println("Enter a grade to search for");
     find = myScanner.nextInt();
        
}// end of main method
        
        
public static void binarySearch(int[] list, int target){
  int counter = 0;
  int first = 0;
  int last = list.length-1;
  int middle = last/2;
  
   while(first <= last){
     counter ++;
     if(target < list[middle]){
    last = middle-1;
     }
     else if (target == list[middle]){
       System.out.println("The grade was found it after " + counter + "iterations");
       return;
     }
     else { 
       first = middle+1;  
     }
     middle = (first + last) /2;
   }// end of while loop
  System.out.println("The grade was not found in the list with" + counter+ "iterations");
   return;
} // end of binary search  
  
public static void randomScrabbling(int[] grades ){
    for(int j = 0; j < 100; j++){
        int index = (int) (Math.random()*15);
        int k = grades[0];
        grades[0] = grades[index];
        grades[index] = k;  
    } // end of for loop
}// end of random scrabbling
 
 public static void linearSearch(int[] list, int target){
    for ( int j = 0; j < 15 ; j++ ){
      if (target == list[j]){
        System.out.println(" we found it after " + j + "iterations");
       return;
      }
    }
    System.out.println("The grade could not be found");
 } // end of linear search   

}// end of class