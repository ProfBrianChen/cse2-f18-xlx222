//////////////////
// CSE002 -HW09
// XIN LING XU 
// NOV.17.18

import java.util.Scanner; //import statement for the scanner class

public class removeElement{
  public static void main(String []args){
	Scanner scan = new Scanner(System.in); // creates a new scanner
int num[] = new int[10]; // created a new array with type integer and having 10 members
int newArray1[]; // creates a new int array named array1
int newArray2[]; // creates a new int array named array2
int index,target; // initialized two new variables as integers
	String answer = ""; // initialized a new string named answer
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out+= listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt(); // obtains user input
  	newArray1 = delete(num,index); //  calls on the delete method
  	String out1 = "The output array is ";
  	out1+= listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target); // calls on the remove method
  	String out2 = "The output array is ";
  	out2+= listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer = scan.next(); // obtains user input
	} while(answer.equals("Y") || answer.equals("y"));
  } // end of main method
 
  
  public static String listArray(int num[]){
	String out = "{";
	for(int j = 0; j < num.length; j++){
  	if(j > 0){
    	out+= ", ";
  	}
  	out+= num[j];
	}
	out+= "} ";
	return out;
  }// end of listArray method
  
  
public static int[] randomInput(){ // method that sets a random integer to each member of the array
  int[] list = new int[10]; // creates a new array with ten elements/members
    for (int i = 0; i < list.length; i++){ // for loop that counts up to the length of the array
      list[i] = (int)(Math.random()*10); // sets each member of the array to a random number between o and 9
    }
  return list; // returns the array names list
}// end of randomInput method
  
  
public static int[] delete(int list[], int index){ // method that deletes a specific member of an array
  int[] array = new int[list.length-1]; // creates a copy array with the length one less than the array that is the input
  int counter = 0; // creates a counter for measurement
  if(!(index < 0) || !(index > list.length)){ //compare the input names index to the length of the array input
    for (int i = 0; i < list.length; i++){ // for loop that couhts up the the length of the input array
      if(i != index){ // array at input index should be deleted so when i doesn't equal index, the counter should be incremented
       counter++;
      }
    }
  }
  return array; // returning the new array without the array member at index
}// end of delete method
  
  
public static int[] remove(int list[], int target){ // method to remove the element that stores a specific integer
  int[] array1 = new int[list.length]; // creates a copy array
    int counter = 0; // counts the iteration of x (on the next line)
    for (int x = 0; x < list.length; x++){ // for loop that counts x up to the length of the input array
      if(array1[x] != target){ // if the target is equal to x, counter is incremented
        counter++;
      }
    }
  return array1; // the new array is being returned
}// end of remove method
  
  
}// end of class


